require 'test_helper'

class WhereCategoriesControllerTest < ActionController::TestCase
  setup do
    @where_category = where_categories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:where_categories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create where_category" do
    assert_difference('WhereCategory.count') do
      post :create, where_category: { description: @where_category.description, title: @where_category.title, what_category_id: @where_category.what_category_id }
    end

    assert_redirected_to where_category_path(assigns(:where_category))
  end

  test "should show where_category" do
    get :show, id: @where_category
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @where_category
    assert_response :success
  end

  test "should update where_category" do
    patch :update, id: @where_category, where_category: { description: @where_category.description, title: @where_category.title, what_category_id: @where_category.what_category_id }
    assert_redirected_to where_category_path(assigns(:where_category))
  end

  test "should destroy where_category" do
    assert_difference('WhereCategory.count', -1) do
      delete :destroy, id: @where_category
    end

    assert_redirected_to where_categories_path
  end
end
