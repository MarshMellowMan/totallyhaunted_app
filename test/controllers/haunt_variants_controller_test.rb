require 'test_helper'

class HauntVariantsControllerTest < ActionController::TestCase
  setup do
    @haunt_variant = haunt_variants(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:haunt_variants)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create haunt_variant" do
    assert_difference('HauntVariant.count') do
      post :create, haunt_variant: { category_id: @haunt_variant.category_id, description: @haunt_variant.description, image: @haunt_variant.image, title: @haunt_variant.title }
    end

    assert_redirected_to haunt_variant_path(assigns(:haunt_variant))
  end

  test "should show haunt_variant" do
    get :show, id: @haunt_variant
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @haunt_variant
    assert_response :success
  end

  test "should update haunt_variant" do
    patch :update, id: @haunt_variant, haunt_variant: { category_id: @haunt_variant.category_id, description: @haunt_variant.description, image: @haunt_variant.image, title: @haunt_variant.title }
    assert_redirected_to haunt_variant_path(assigns(:haunt_variant))
  end

  test "should destroy haunt_variant" do
    assert_difference('HauntVariant.count', -1) do
      delete :destroy, id: @haunt_variant
    end

    assert_redirected_to haunt_variants_path
  end
end
