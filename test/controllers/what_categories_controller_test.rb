require 'test_helper'

class WhatCategoriesControllerTest < ActionController::TestCase
  setup do
    @what_category = what_categories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:what_categories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create what_category" do
    assert_difference('WhatCategory.count') do
      post :create, what_category: { description: @what_category.description, title: @what_category.title }
    end

    assert_redirected_to what_category_path(assigns(:what_category))
  end

  test "should show what_category" do
    get :show, id: @what_category
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @what_category
    assert_response :success
  end

  test "should update what_category" do
    patch :update, id: @what_category, what_category: { description: @what_category.description, title: @what_category.title }
    assert_redirected_to what_category_path(assigns(:what_category))
  end

  test "should destroy what_category" do
    assert_difference('WhatCategory.count', -1) do
      delete :destroy, id: @what_category
    end

    assert_redirected_to what_categories_path
  end
end
