require 'test_helper'

class WhenCategoriesControllerTest < ActionController::TestCase
  setup do
    @when_category = when_categories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:when_categories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create when_category" do
    assert_difference('WhenCategory.count') do
      post :create, when_category: { description: @when_category.description, title: @when_category.title, where_category_id: @when_category.where_category_id }
    end

    assert_redirected_to when_category_path(assigns(:when_category))
  end

  test "should show when_category" do
    get :show, id: @when_category
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @when_category
    assert_response :success
  end

  test "should update when_category" do
    patch :update, id: @when_category, when_category: { description: @when_category.description, title: @when_category.title, where_category_id: @when_category.where_category_id }
    assert_redirected_to when_category_path(assigns(:when_category))
  end

  test "should destroy when_category" do
    assert_difference('WhenCategory.count', -1) do
      delete :destroy, id: @when_category
    end

    assert_redirected_to when_categories_path
  end
end
