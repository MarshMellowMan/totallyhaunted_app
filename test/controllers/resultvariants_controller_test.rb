require 'test_helper'

class ResultvariantsControllerTest < ActionController::TestCase
  setup do
    @resultvariant = resultvariants(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:resultvariants)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create resultvariant" do
    assert_difference('Resultvariant.count') do
      post :create, resultvariant: { description: @resultvariant.description, image: @resultvariant.image, symptom_category_id: @resultvariant.symptom_category_id, title: @resultvariant.title }
    end

    assert_redirected_to resultvariant_path(assigns(:resultvariant))
  end

  test "should show resultvariant" do
    get :show, id: @resultvariant
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @resultvariant
    assert_response :success
  end

  test "should update resultvariant" do
    patch :update, id: @resultvariant, resultvariant: { description: @resultvariant.description, image: @resultvariant.image, symptom_category_id: @resultvariant.symptom_category_id, title: @resultvariant.title }
    assert_redirected_to resultvariant_path(assigns(:resultvariant))
  end

  test "should destroy resultvariant" do
    assert_difference('Resultvariant.count', -1) do
      delete :destroy, id: @resultvariant
    end

    assert_redirected_to resultvariants_path
  end
end
