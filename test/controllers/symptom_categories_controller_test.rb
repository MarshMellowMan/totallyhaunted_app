require 'test_helper'

class SymptomCategoriesControllerTest < ActionController::TestCase
  setup do
    @symptom_category = symptom_categories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:symptom_categories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create symptom_category" do
    assert_difference('SymptomCategory.count') do
      post :create, symptom_category: { description: @symptom_category.description, image: @symptom_category.image, title: @symptom_category.title, when_category_id: @symptom_category.when_category_id }
    end

    assert_redirected_to symptom_category_path(assigns(:symptom_category))
  end

  test "should show symptom_category" do
    get :show, id: @symptom_category
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @symptom_category
    assert_response :success
  end

  test "should update symptom_category" do
    patch :update, id: @symptom_category, symptom_category: { description: @symptom_category.description, image: @symptom_category.image, title: @symptom_category.title, when_category_id: @symptom_category.when_category_id }
    assert_redirected_to symptom_category_path(assigns(:symptom_category))
  end

  test "should destroy symptom_category" do
    assert_difference('SymptomCategory.count', -1) do
      delete :destroy, id: @symptom_category
    end

    assert_redirected_to symptom_categories_path
  end
end
