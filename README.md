# README #

TotallyHaunted App

Determine if you, your home, or your friends are totally haunted. Based on real-life information from fictional horror movies.

Overview

If you are any type of female between 13 and 30, or simply a male who slept with the lights on after watching paranormal activity, TotallyHaunted is the one-stop app to completely dispel (or verify) your conviction that you are being haunted by entities from the great beyond. Based on real-life information from fictional horror movies, TotallyHaunted will help you diagnose your particular blend of haunting, from your mischievous poltergeist to murderous, unstoppable juggernauts wearing hockey masks. 

Lights flickering? Dark Shadows moving around your bedroom? Toilet paper completely gone when you need it most? TotallyHaunted can be the difference between calling in a priest, checking into a mental hospital, or just kicking out your roommate Steve. Rely on TotallyHaunted to diagnose your specific haunting. 



This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact