class WhereCategoriesController < ApplicationController
  before_action :set_where_category, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @where_categories = WhereCategory.all
    respond_with(@where_categories)
  end

  def show
    respond_with(@where_category)
  end

  def new
    @where_category = WhereCategory.new
    @where_category.what_category_id = params[:what_category_id]
  end

  def edit
  end

  def create
    @where_category = WhereCategory.new(where_category_params)
    @where_category.save
    respond_with(@where_category)
  end

  def update
    @where_category.update(where_category_params)
    respond_with(@where_category)
  end

  def destroy
    @where_category.destroy
    respond_with(@where_category)
  end

  private
    def set_where_category
      @where_category = WhereCategory.find(params[:id])
    end

    def where_category_params
      params.require(:where_category).permit(:title, :description, :what_category_id)
    end
end
