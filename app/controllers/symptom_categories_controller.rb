class SymptomCategoriesController < ApplicationController
  before_action :set_symptom_category, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @symptom_categories = SymptomCategory.all
    respond_with(@symptom_categories)
  end

  def show
    respond_with(@symptom_category)
  end

  def new
    @symptom_category = SymptomCategory.new
    @symptom_category.when_category_id = params[:when_category_id]
    # respond_with(@symptom_category)
  end

  def edit
  end

  def create
    @symptom_category = SymptomCategory.new(symptom_category_params)
    @symptom_category.save
    respond_with(@symptom_category)
  end

  def update
    @symptom_category.update(symptom_category_params)
    respond_with(@symptom_category)
  end

  def destroy
    @symptom_category.destroy
    respond_with(@symptom_category)
  end

  private
    def set_symptom_category
      @symptom_category = SymptomCategory.find(params[:id])
    end

    def symptom_category_params
      params.require(:symptom_category).permit(:title, :description, :when_category_id, :image)
    end
end
