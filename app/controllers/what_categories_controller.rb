class WhatCategoriesController < ApplicationController
  before_action :set_what_category, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @what_categories = WhatCategory.all
    respond_with(@what_categories)
  end

  def show
    respond_with(@what_category)
  end

  def new
    @what_category = WhatCategory.new
    respond_with(@what_category)
  end

  def edit
  end

  def create
    @what_category = WhatCategory.new(what_category_params)
    @what_category.save
    respond_with(@what_category)
  end

  def update
    @what_category.update(what_category_params)
    respond_with(@what_category)
  end

  def destroy
    @what_category.destroy
    respond_with(@what_category)
  end

  private
    def set_what_category
      @what_category = WhatCategory.find(params[:id])
    end

    def what_category_params
      params.require(:what_category).permit(:title, :description)
    end
end
