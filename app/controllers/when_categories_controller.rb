class WhenCategoriesController < ApplicationController
  before_action :set_when_category, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @when_categories = WhenCategory.all
    respond_with(@when_categories)
  end

  def show
    respond_with(@when_category)
  end

  def new
    @when_category = WhenCategory.new
    @when_category.where_category_id = params[:where_category_id]
    # respond_with(@when_category)
  end

  def edit
  end

  def create
    @when_category = WhenCategory.new(when_category_params)
    @when_category.save
    respond_with(@when_category)
  end

  def update
    @when_category.update(when_category_params)
    respond_with(@when_category)
  end

  def destroy
    @when_category.destroy
    respond_with(@when_category)
  end

  private
    def set_when_category
      @when_category = WhenCategory.find(params[:id])
    end

    def when_category_params
      params.require(:when_category).permit(:title, :description, :where_category_id)
    end
end
