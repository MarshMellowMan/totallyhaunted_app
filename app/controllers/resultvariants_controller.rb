class ResultvariantsController < ApplicationController
  before_action :set_resultvariant, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @resultvariants = Resultvariant.all
    respond_with(@resultvariants)
  end

  def show
    respond_with(@resultvariant)
  end

  def new
    @resultvariant = Resultvariant.new
    @resultvariant.symptom_category_id = params[:symptom_category_id]
    # @symptom_category.when_category_id = params[:when_category_id]
    # respond_with(@resultvariant)
  end

  def edit
  end

  def create
    @resultvariant = Resultvariant.new(resultvariant_params)
    @resultvariant.save
    respond_with(@resultvariant)
  end

  def update
    @resultvariant.update(resultvariant_params)
    respond_with(@resultvariant)
  end

  def destroy
    @resultvariant.destroy
    respond_with(@resultvariant)
  end

  private
    def set_resultvariant
      @resultvariant = Resultvariant.find(params[:id])
    end

    def resultvariant_params
      params.require(:resultvariant).permit(:title, :description, :symptom_category_id, :image)
    end
end
