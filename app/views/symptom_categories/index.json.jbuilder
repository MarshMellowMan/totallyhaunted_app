json.array!(@symptom_categories) do |symptom_category|
  json.extract! symptom_category, :id, :title, :description, :when_category_id, :image
  json.url symptom_category_url(symptom_category, format: :json)
end
