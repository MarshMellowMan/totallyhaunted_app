json.array!(@what_categories) do |what_category|
  json.extract! what_category, :id, :title, :description
  json.url what_category_url(what_category, format: :json)
end
