json.array!(@resultvariants) do |resultvariant|
  json.extract! resultvariant, :id, :title, :description, :symptom_category_id, :image
  json.url resultvariant_url(resultvariant, format: :json)
end
