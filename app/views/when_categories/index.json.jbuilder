json.array!(@when_categories) do |when_category|
  json.extract! when_category, :id, :title, :description, :where_category_id
  json.url when_category_url(when_category, format: :json)
end
