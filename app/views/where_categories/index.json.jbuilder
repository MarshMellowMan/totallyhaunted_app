json.array!(@where_categories) do |where_category|
  json.extract! where_category, :id, :title, :description, :what_category_id
  json.url where_category_url(where_category, format: :json)
end
