class WhereCategory < ActiveRecord::Base
  belongs_to :what_category
  has_many :when_categories
end
