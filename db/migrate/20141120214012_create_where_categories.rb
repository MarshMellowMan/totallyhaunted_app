class CreateWhereCategories < ActiveRecord::Migration
  def change
    create_table :where_categories do |t|
      t.string :title
      t.text :description
      t.integer :what_category_id

      t.timestamps
    end
  end
end
