class CreateWhatCategories < ActiveRecord::Migration
  def change
    create_table :what_categories do |t|
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
