class CreateResultvariants < ActiveRecord::Migration
  def change
    create_table :resultvariants do |t|
      t.string :title
      t.text :description
      t.integer :symptom_category_id
      t.string :image

      t.timestamps
    end
  end
end
