class CreateWhenCategories < ActiveRecord::Migration
  def change
    create_table :when_categories do |t|
      t.string :title
      t.text :description
      t.integer :where_category_id

      t.timestamps
    end
  end
end
