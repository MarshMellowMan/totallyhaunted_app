class CreateSymptomCategories < ActiveRecord::Migration
  def change
    create_table :symptom_categories do |t|
      t.string :title
      t.text :description
      t.integer :when_category_id
      t.string :image

      t.timestamps
    end
  end
end
